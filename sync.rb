# Note: 
# - code hasn't been tested
# - once tested, can be schedule to be run as a cron

require 'open3'

def run_command(cmd, match_output_text=nil)    
    stdout, stderr, status = Open3.capture3(cmd)

    if !status.success? || (!match_output_text.nil? && !stdout.include?(match_output_text))
        puts "STDOUT: #{stdout}"
        puts "STDERR: #{stderr}"
        return false
    end
        
    status.success?
end

if run_command_or_exit(cmd, match_output_text=nil)
    exit unless run_command(cmd, match_output_text)
    # TODO: or email to dev group
end

# TODO: cd to source code repository

run_command_or_exit "git reset —hard"

run_command_or_exit "git pull master"

run_command_or_exit "theme get —env=production"
# ensure the config.yaml has production confgs (theme, api secret, store)

not_current = run_command_or_exit "git st", "Your branch is up-to-date"

if not_current
    # Either notify the dev group or automate
    run_command "git add ."
    run_command "git commit -m'Updates from Shopify admin UI edited changes'"
    run_command_or_exit "git push origin master"
end
